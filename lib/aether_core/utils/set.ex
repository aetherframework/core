defmodule Aether.Core.Utils.Set do
  @moduledoc """
  Utillity module for MapSet manipulation
  """

  @doc """
  Returns a new set

  ## Examples

      iex> Aether.Core.Utils.Set.new_set()
      #MapSet<[]>

  """
  def new_set, do: new_set([])

  @doc """
  Returns a new set with the passed contents

  ## Examples

      iex> Aether.Core.Utils.Set.new_set(:foo)
      #MapSet<[:foo]>

      iex> Aether.Core.Utils.Set.new_set([:foo, :bar])
      #MapSet<[:bar, :foo]>

      iex> Aether.Core.Utils.Set.new_set([:foo, :foo])
      #MapSet<[:foo]>

  """
  def new_set(contents)
  def new_set(%MapSet{} = set), do: set
  def new_set(value) when not is_list(value), do: new_set([value])
  def new_set(values), do: values |> Enum.into(MapSet.new())

  @doc """
  Returns a new set with the passed items

  ## Examples

      iex> Aether.Core.Utils.Set.put_set(MapSet.new([:foo]), :bar)
      #MapSet<[:bar, :foo]>

      iex> Aether.Core.Utils.Set.put_set(MapSet.new([:foo]), [:bar, :baz])
      #MapSet<[:bar, :baz, :foo]>

      iex> Aether.Core.Utils.Set.put_set(MapSet.new([:foo]), [:foo])
      #MapSet<[:foo]>

  """
  def put_set(set, insert)
  def put_set(%MapSet{} = s1, %MapSet{} = s2), do: MapSet.union(s2, s1)
  def put_set(%MapSet{} = set, values), do: put_set(set, new_set(values))

  @doc """
  Drops the passed items from the set

  ## Examples

      iex> Aether.Core.Utils.Set.drop_set(MapSet.new([:foo, :bar]), :bar)
      #MapSet<[:foo]>

      iex> Aether.Core.Utils.Set.drop_set(MapSet.new([:foo, :bar]), [:foo, :bar])
      #MapSet<[]>

      iex> Aether.Core.Utils.Set.drop_set(MapSet.new([:foo, :bar]), :baz)
      #MapSet<[:bar, :foo]>

  """
  def drop_set(set, drop)

  def drop_set(%MapSet{} = set, %MapSet{} = drop),
    do: MapSet.difference(set, drop)

  def drop_set(%MapSet{} = set, drop), do: drop_set(set, new_set(drop))

  @doc """
  Maps the values of the set with the given function

  ## Examples

      iex> Aether.Core.Utils.Set.map_set(MapSet.new([1, 2]), fn val -> val + 1 end)
      #MapSet<[2, 3]>

      iex> Aether.Core.Utils.Set.map_set(MapSet.new([-2, 2]), &:math.pow(&1, 2))
      #MapSet<[4.0]>

  """
  def map_set(%MapSet{} = set, fun), do: set |> Enum.map(fun) |> new_set()

  @doc """
  Selects the values of the set with the given predicate function

  ## Examples

      iex> Aether.Core.Utils.Set.filter_set(MapSet.new([:foo, :bar]), &(&1 == :bar))
      #MapSet<[:bar]>

  """
  def filter_set(%MapSet{} = set, fun), do: set |> Enum.filter(fun) |> new_set()

  @doc """
  Rejects the values of the set with the given predicate function

  ## Examples

      iex> Aether.Core.Utils.Set.reject_set(MapSet.new([:foo, :bar]), &(&1 == :bar))
      #MapSet<[:foo]>

  """
  def reject_set(%MapSet{} = set, fun),
    do: set |> filter_set(fun) |> (&drop_set(set, &1)).()
end

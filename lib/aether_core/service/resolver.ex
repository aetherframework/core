defmodule Aether.Core.Service.Resolver do
  @moduledoc """
  Service Resolver module
  """

  alias Aether.Core.Service.{Group, Registrar}

  def sigil_p(path, []), do: String.replace(path, " ", "/")

  def resolve_service(service_group, service_name) when is_binary(service_group) do
    with {:ok, group} <- resolve_group(service_group),
         {:ok, service} <- resolve_service(group, service_name) do
      {:ok, service}
    else
      {:group_not_found, _} -> {:error, "Group not found #{service_group}"}
      {:service_not_found, _} -> {:error, "Service not found #{service_name}"}
    end
  end

  def resolve_service(group, service_path) when is_pid(group) do
    case group_lookup(group, service_path) do
      pid when is_pid(pid) -> {:ok, pid}
      nil -> {:service_not_found, "#{service_path}"}
      _ -> {:error, "Unknown error occurred looking up #{service_path}"}
    end
  end

  def resolve_service!(group, service_path) do
    {:ok, pid} = resolve_service(group, service_path)
    pid
  end

  def via_service_group(service_group, service_path),
    do: {:via, Registrar.Server, {Registrar.Server, service_tuple(service_group, service_path)}}

  defp resolve_group(service_group) do
    case Registrar.Server.lookup(Registrar.Server, service_group) do
      pid when is_pid(pid) -> {:ok, pid}
      nil -> {:group_not_found, service_group}
      _ -> {:error, "Unkown error occurred looking up #{service_group}"}
    end
  end

  defp group_name(pid) when is_pid(pid), do: Group.Server.name(pid)
  defp group_name(%Group{name: name}), do: name

  defp group_lookup(pid, service_path),
    do: Group.Server.lookup(pid, service_path)

  defp service_tuple(group_pid, service_path) when is_pid(group_pid),
    do: group_pid |> group_name |> service_tuple(service_path)

  defp service_tuple(%Group{} = group, service_path),
    do: group |> group_name |> service_tuple(service_path)

  defp service_tuple(service_group, service_path),
    do: {service_group, service_path}
end

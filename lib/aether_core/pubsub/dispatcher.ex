defprotocol Aether.Core.PubSub.Dispatcher do
  @doc "Calls the passed list of event handlers"
  def dispatch(dispatcher, handlers)
end

defmodule Aether.Core.Configurable do
  @moduledoc """
  This module provides macros to define config structs
  for modules
  """

  defmacro defoptions(block) do
    block = block |> Macro.escape()

    quote do
      Module.put_attribute(__MODULE__, :options_fields, unquote(block))

      @before_compile Aether.Core.Configurable
    end
  end

  defmacro __before_compile__(env) do
    block = Module.get_attribute(env.module, :options_fields)

    quote do
      defmodule __MODULE__.Options do
        alias unquote(env.module)

        require Specify

        Specify.defconfig(unquote(block))

        def new(values \\ [], opts \\ []),
          do: load_explicit(values, opts)
      end
    end
  end
end

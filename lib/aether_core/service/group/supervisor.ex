defmodule Aether.Core.Service.Group.Supervisor do
  @moduledoc false

  alias Aether.Core.Service.Group

  use Supervisor

  def start_link(group: group_name) do
    group = Group.new(group_name)

    Supervisor.start_link(__MODULE__, group: group)
  end

  def init(group: group) do
    children = [
      {Group.Server, [group: group]}
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end

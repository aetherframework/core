defmodule Aether.Core.PubSub.Dispatcher.SyncTest do
  @moduledoc false

  use ExUnit.Case, async: true

  alias Aether.Core.PubSub.Dispatcher
  alias Aether.Core.PubSub.Dispatcher.Sync

  doctest Sync

  setup do
    dispatcher = Sync.new()

    {:ok, %{dispatcher: dispatcher}}
  end

  describe "Aether.Core.PubSub.Dispatcher.Sync.dispatch/2" do
    test "returns empty list when handlers list is empty", %{dispatcher: disp} do
      result = Sync.dispatch(disp, [])

      assert result == []
    end

    test "returns list of handler return values", %{dispatcher: disp} do
      handlers = [fn -> :foo end, fn -> :bar end]
      result = Sync.dispatch(disp, handlers)

      assert result == [:foo, :bar]
    end

    test "raises ArgumentError when passed invalid handlers value", %{dispatcher: disp} do
      assert_raise ArgumentError, "Invalid value for handlers passed", fn ->
        Sync.dispatch(disp, :invalid)
      end
    end
  end

  describe "Aether.Core.PubSub.Dispatcher.dispatch/2" do
    test "returns empty list when handlers list is empty", %{dispatcher: disp} do
      result = Dispatcher.dispatch(disp, [])

      assert result == []
    end

    test "returns list of handler return values", %{dispatcher: disp} do
      handlers = [fn -> :foo end, fn -> :bar end]
      result = Dispatcher.dispatch(disp, handlers)

      assert result == [:foo, :bar]
    end

    test "raises ArgumentError when passed invalid handlers value", %{dispatcher: disp} do
      assert_raise ArgumentError, "Invalid value for handlers passed", fn ->
        Dispatcher.dispatch(disp, :invalid)
      end
    end
  end
end

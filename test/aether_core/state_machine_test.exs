defmodule Aether.Core.StateMachineTests do
  @moduledoc false

  alias Aether.Core.StateMachine

  use ExUnit.Case, async: true
  doctest StateMachine

  defmodule TestMachine do
    @moduledoc false

    alias Aether.Core.StateMachine

    use StateMachine

    def start_link(args),
      do: StateMachine.start_link(__MODULE__, args)

    def init({state, data}), do: {:ok, state, data}

    def handle_cast(:toggle, :off, data),
      do: {:noreply, {:transition, :on}, data, {:continue, :inc}}

    def handle_cast(:toggle, :on, data),
      do: {:noreply, {:transition, :off}, data, {:continue, :dec}}

    def handle_cast(:toggle_with_state_timeout, :off, data),
      do: {:noreply, {:transition, :on}, data, {:state_timeout, :turn_off, 250}}

    def handle_cast(:toggle_with_state_timeout, :on, _data),
      do: {:noreply, :keep, :keep, {:state_timeout, :turn_off, 250}}

    def handle_cast(:cancel_state_timeout, _, _),
      do: {:noreply, :keep, :keep, {:state_timeout, :cancel}}

    def handle_cast(:postpone_turn_off, :off, _data),
      do: {:noreply, :keep, :keep, :postpone}

    def handle_cast(:postpone_turn_off, :on, data),
      do: {:noreply, {:transition, :off}, data}

    def handle_cast(:trigger_internal_event, _, _),
      do: {:noreply, :keep, :keep, {:internal, :set_data}}

    def handle_cast(:dim, _, data),
      do: {:noreply, {:transition, :dimmed}, data}

    def handle_cast(:turn_on, :on, _data),
      do: {:noreply, :keep, :keep}

    def handle_cast(:turn_on, _, data),
      do: {:noreply, {:transition, :on}, data, {:continue, :inc}}

    def handle_cast(:turn_off, :off, _data),
      do: {:noreply, :keep, :keep}

    def handle_cast(:turn_off, _, data),
      do: {:noreply, {:transition, :off}, data, {:continue, :dec}}

    def handle_call(:state, _from, state, _data),
      do: {:reply, state, :keep, :keep}

    def handle_call(:data, _from, _state, data),
      do: {:reply, data, :keep, :keep}

    def handle_continue(:dec, _state, data),
      do: {:noreply, :keep, data - 1}

    def handle_continue(:inc, _state, data),
      do: {:noreply, :keep, data + 1}

    def handle_internal(:set_data, _state, _data),
      do: {:noreply, :keep, :set_by_internal}

    def handle_timeout({:state, :turn_off}, :on, data),
      do: {:noreply, {:transition, :off}, data}
  end

  describe "Aether.Core.StateMachine.start_link/2" do
    test "initializes state machine in :off state" do
      {:ok, pid} = TestMachine.start_link({:off, 0})

      assert StateMachine.call(pid, :state) == :off
    end

    test "initializes state machine data to 0" do
      {:ok, pid} = TestMachine.start_link({:off, 0})

      assert StateMachine.call(pid, :data) == 0
    end
  end

  describe "Aether.Core.StateMachine.cast/2" do
    test "changes state machine's state to :on" do
      {:ok, pid} = TestMachine.start_link({:off, 0})

      StateMachine.cast(pid, :toggle)

      assert StateMachine.call(pid, :state) == :on
    end

    test "increments state machine's data by 1" do
      {:ok, pid} = TestMachine.start_link({:off, 0})

      StateMachine.cast(pid, :toggle)

      assert StateMachine.call(pid, :data) == 1
    end

    test "changes state machine's state to :on and back to :off" do
      {:ok, pid} = TestMachine.start_link({:off, 0})

      StateMachine.cast(pid, :toggle)
      StateMachine.cast(pid, :toggle)

      assert StateMachine.call(pid, :state) == :off
    end

    test "changes state machine's data back to 0" do
      {:ok, pid} = TestMachine.start_link({:off, 0})

      StateMachine.cast(pid, :toggle)
      StateMachine.cast(pid, :toggle)

      assert StateMachine.call(pid, :data) == 0
    end
  end

  describe "Aether.Core.StateMachine.handle_timeout/3" do
    test "state timeout is triggered and changes state" do
      {:ok, pid} = TestMachine.start_link({:on, 1})

      StateMachine.cast(pid, :toggle_with_state_timeout)

      Process.sleep(260)

      assert StateMachine.call(pid, :state) == :off
    end

    test "state timeout is not triggered and does not change state" do
      {:ok, pid} = TestMachine.start_link({:on, 1})

      StateMachine.cast(pid, :toggle_with_state_timeout)
      StateMachine.cast(pid, :dim)

      Process.sleep(260)

      assert StateMachine.call(pid, :state) == :dimmed
    end

    test "state timeout is cancelled and does not change state" do
      {:ok, pid} = TestMachine.start_link({:on, 1})

      StateMachine.cast(pid, :toggle_with_state_timeout)
      StateMachine.cast(pid, :cancel_state_timeout)

      Process.sleep(260)

      assert StateMachine.call(pid, :state) == :on
    end
  end

  describe "Postponing events" do
    test "postponed event isn't invoked until state change" do
      {:ok, pid} = TestMachine.start_link({:off, 0})

      StateMachine.cast(pid, :postpone_turn_off)

      assert StateMachine.call(pid, :data) == 0
      assert StateMachine.call(pid, :state) == :off

      StateMachine.cast(pid, :toggle)

      assert StateMachine.call(pid, :state) == :off
    end
  end

  describe "Aether.Core.StateMachine.handle_internal/3" do
    test "internal event is triggered" do
      {:ok, pid} = TestMachine.start_link({:off, 0})

      StateMachine.cast(pid, :trigger_internal_event)

      assert StateMachine.call(pid, :data) == :set_by_internal
    end
  end
end

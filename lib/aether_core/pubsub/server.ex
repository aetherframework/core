defmodule Aether.Core.PubSub.Server do
  @moduledoc false

  alias Aether.Core.PubSub.{Bus, Event}
  alias Aether.Core.Service.Resolver

  use GenServer

  require Logger

  import Resolver.Process

  def start_link([group: group, dispatcher: _] = opts) do
    name = via_service_group(group, ~p/pubsub/)

    GenServer.start_link(__MODULE__, opts, name: name)
  end

  def init(group: group, dispatcher: dispatcher) do
    service_group_set(group)

    bus = Bus.new(dispatcher)
    state = %{bus: bus, refs: %{}, group: group}

    {:ok, state}
  end

  def subscribe(server, topic, handler),
    do: GenServer.cast(server, {:subscribe, topic, self(), handler})

  def unsubscribe(server),
    do: GenServer.cast(server, {:unsubscribe, self()})

  def unsubscribe(server, topics),
    do: GenServer.cast(server, {:unsubscribe, self(), topics})

  def dispatch(server, %Event{} = event),
    do: GenServer.cast(server, {:dispatch, event})

  def dispatch(server, topic, payload),
    do: GenServer.cast(server, {:dispatch, Event.new(topic, payload)})

  def topics(server), do: topics(server, self())

  def topics(server, pid),
    do: GenServer.call(server, {:topics, pid})

  def state(server), do: GenServer.call(server, :state)

  def handle_cast({:subscribe, topic, pid, handler}, %{bus: bus, refs: refs} = state) do
    bus = Bus.subscribe(bus, topic, pid, handler)
    refs = Map.put(refs, Process.monitor(pid), pid)

    {:noreply, %{state | bus: bus, refs: refs}}
  end

  def handle_cast({:unsubscribe, pid}, %{bus: bus, refs: refs} = state) do
    bus = Bus.unsubscribe(bus, pid)
    refs = demonitor(pid, refs)

    {:noreply, %{state | bus: bus, refs: refs}}
  end

  def handle_cast({:unsubscribe, pid, topics}, %{bus: bus} = state),
    do: {:noreply, %{state | bus: Bus.unsubscribe(bus, pid, topics)}}

  def handle_cast({:dispatch, %Event{} = event}, %{bus: bus} = state) do
    Bus.dispatch(bus, event)

    {:noreply, state}
  end

  def handle_call({:topics, _pid}, _from, %{bus: bus} = state),
    do: {:reply, Bus.topics(bus), state}

  def handle_call(:state, _from, state), do: {:reply, state, state}

  def handle_info({:EXIT, _, :normal}, state), do: {:noreply, state}

  def handle_info({:DOWN, ref, :process, pid, _}, %{bus: bus, refs: refs} = state) do
    bus = Bus.unsubscribe(bus, pid)
    refs = demonitor(ref, refs)

    {:noreply, %{state | bus: bus, refs: refs}}
  end

  def handle_info(_, state), do: {:noreply, state}

  defp find_ref(pid, refs) do
    case Enum.find(refs, fn {_ref, ref_pid} -> ref_pid == pid end) do
      {ref, ^pid} when is_reference(ref) -> ref
      _ -> nil
    end
  end

  defp demonitor(ref, refs) when is_reference(ref),
    do: Process.demonitor(ref) && Map.drop(refs, [ref])

  defp demonitor(pid, refs) when is_pid(pid),
    do: pid |> find_ref(refs) |> demonitor(refs)
end

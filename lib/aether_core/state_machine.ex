defmodule Aether.Core.StateMachine do
  @moduledoc """
  A behaviour that wraps the `GenStateMachine` behaviour to provide
  and interface similiar to `GenServer` by wrapping the
  `GenStateMachine.handle_event` callback to allow for `handle_cast`,
  `handle_call`, and `handle_info` to be used.
  """

  @callback handle_call(event :: term(), from :: pid(), state :: atom(), data :: term()) ::
              :gen_statem.event_handler_result(GenStateMachine.state())

  @callback handle_cast(event :: term(), state :: atom(), data :: term()) ::
              :gen_statem.event_handler_result(GenStateMachine.state())

  @callback handle_info(event :: term(), state :: atom(), data :: term()) ::
              :gen_statem.event_handler_result(GenStateMachine.state())

  @callback handle_internal(event :: term(), state :: atom(), data :: term()) ::
              :gen_statem.event_handler_result(GenStateMachine.state())

  @callback handle_timeout(event :: term(), state :: atom(), data :: term()) ::
              :gen_statem.event_handler_result(GenStateMachine.state())

  @callback handle_continue(event :: term(), state :: atom(), data :: term()) ::
              :gen_statem.event_handler_result(GenStateMachine.state())

  @optional_callbacks handle_call: 4,
                      handle_cast: 3,
                      handle_info: 3,
                      handle_internal: 3,
                      handle_timeout: 3,
                      handle_continue: 3

  defmacro __using__(opts \\ []) do
    quote location: :keep, bind_quoted: [opts: opts, module: __CALLER__.module] do
      alias Aether.Core.StateMachine

      use GenStateMachine

      def start_link(opts \\ []), do: start_link(nil, opts)

      def start_link(initializer, opts),
        do: StateMachine.start_link(__MODULE__, initializer, opts)

      def handle_call(_, _, _, _), do: {:noreply, :keep_state_and_data}
      def handle_cast(_, _, _), do: {:noreply, :keep_state_and_data}
      def handle_info(_, _, _), do: {:noreply, :keep_state_and_data}
      def handle_internal(_, _, _), do: {:noreply, :keep_state_and_data}
      def handle_timeout(_, _, _), do: {:noreply, :keep_state_and_data}
      def handle_continue(_, _, _), do: {:noreply, :keep_state_and_data}

      def handle_event(type, event, state, data),
        do: StateMachine.handle_event(__MODULE__, type, event, state, data)

      @doc """
      Returns a specification to start this module under a supervisor.
      See `Supervisor` in Elixir v1.6+.
      """
      def child_spec(arg) do
        default = %{id: __MODULE__, start: {__MODULE__, :start_link, [arg]}}

        Enum.reduce(unquote(opts), default, fn
          {key, value}, acc when key in [:id, :type, :restart, :shutdown] ->
            Map.put(acc, key, value)

          {key, _value}, _acc ->
            raise ArgumentError, "key #{inspect(key)} in child spec override"
        end)
      end

      defoverridable start_link: 1,
                     start_link: 2,
                     handle_call: 4,
                     handle_cast: 3,
                     handle_info: 3,
                     handle_internal: 3,
                     handle_timeout: 3,
                     handle_continue: 3,
                     child_spec: 1
    end
  end

  def start_link(module, args, opts \\ []),
    do: GenStateMachine.start_link(module, args, opts)

  def call(pid, event),
    do: GenStateMachine.call(pid, event)

  def cast(pid, event),
    do: GenStateMachine.cast(pid, event)

  def info(pid, event), do: send(pid, event)

  def actions(actions), do: build_actions(actions)

  @doc false
  def handle_event(module, type, event, state, data) when is_atom(module),
    do: do_handle_event(module, type, event, state, data)

  defp do_handle_event(module, {:call, from}, event, state, data) do
    event
    |> module.handle_call(from, state, data)
    |> callback_response(from)
  end

  defp do_handle_event(module, :cast, event, state, data) do
    event
    |> module.handle_cast(state, data)
    |> callback_response()
  end

  defp do_handle_event(module, :info, {:"$continue", event}, state, data) do
    event
    |> module.handle_continue(state, data)
    |> callback_response()
  end

  defp do_handle_event(module, :info, event, state, data) do
    event
    |> module.handle_info(state, data)
    |> callback_response()
  end

  defp do_handle_event(module, :internal, event, state, data) do
    event
    |> module.handle_internal(state, data)
    |> callback_response()
  end

  defp do_handle_event(module, :timeout, event, state, data) do
    {:timeout, event}
    |> module.handle_timeout(state, data)
    |> callback_response()
  end

  defp do_handle_event(module, :state_timeout, event, state, data) do
    {:state, event}
    |> module.handle_timeout(state, data)
    |> callback_response()
  end

  defp do_handle_event(module, {:timeout, name}, event, state, data) do
    {name, event}
    |> module.handle_timeout(state, data)
    |> callback_response()
  end

  defp callback_response(response, from \\ nil),
    do: transform_response(response, from)

  defp transform_response({:reply, reply, {:transition, state}, data}, from),
    do: {:next_state, state, data, [{:reply, from, reply}]}

  defp transform_response({:reply, reply, {:transition, state}, data, actions}, from),
    do: {:next_state, state, data, [{:reply, from, reply} | build_actions(actions)]}

  defp transform_response({:reply, reply, :keep, :keep}, from),
    do: {:keep_state_and_data, [{:reply, from, reply}]}

  defp transform_response({:reply, reply, :keep, data}, from),
    do: {:keep_state, data, [{:reply, from, reply}]}

  defp transform_response({:reply, reply, :keep, :keep, actions}, from),
    do: {:keep_state_and_data, [{:reply, from, reply} | build_actions(actions)]}

  defp transform_response({:reply, reply, :keep, data, actions}, from),
    do: {:keep_state, data, [{:reply, from, reply} | build_actions(actions)]}

  defp transform_response({:noreply, {:transition, state}, data}, _),
    do: {:next_state, state, data}

  defp transform_response({:noreply, {:transition, state}, data, actions}, _),
    do: {:next_state, state, data, build_actions(actions)}

  defp transform_response({:noreply, :keep, :keep}, _),
    do: :keep_state_and_data

  defp transform_response({:noreply, :keep, data}, _),
    do: {:keep_state, data}

  defp transform_response({:noreply, :keep, :keep, actions}, _),
    do: {:keep_state_and_data, build_actions(actions)}

  defp transform_response({:noreply, :keep, data, actions}, _),
    do: {:keep_state, data, build_actions(actions)}

  defp build_actions(action) when not is_list(action),
    do: build_actions([action])

  defp build_actions(actions),
    do: Enum.map(actions, &build_action/1)

  defp build_action({:state_timeout, :update, event}),
    do: {:state_timeout, :update, event}

  defp build_action({:state_timeout, :cancel}),
    do: {:state_timeout, :infinity, nil}

  defp build_action({:state_timeout, event, time}),
    do: {:state_timeout, time, event}

  defp build_action({:state_timeout, event, time, opts}),
    do: {:state_timeout, time, event, opts}

  defp build_action({{:timeout, name}, :update, event}),
    do: {{:timeout, name}, :update, event}

  defp build_action({{:timeout, name}, :cancel}),
    do: {{:timeout, name}, :cancel}

  defp build_action({{:timeout, name}, event, time}),
    do: {{:timeout, name}, time, event}

  defp build_action({{:timeout, name}, event, time, opts}),
    do: {{:timeout, name}, time, event, opts}

  defp build_action({:timeout, time, event}),
    do: {:timeout, time, event}

  defp build_action({:timeout, time, event, opts}),
    do: {:timeout, time, event, opts}

  defp build_action({:continue, event}),
    do: {:next_event, :info, {:"$continue", event}}

  defp build_action({:event, event}),
    do: {:next_event, :cast, event}

  defp build_action({:cast, event}),
    do: {:next_event, :cast, event}

  defp build_action({:call, from, event}),
    do: {:next_event, {:call, from}, event}

  defp build_action({:info, event}),
    do: {:next_event, :info, event}

  defp build_action({:internal, event}),
    do: {:next_event, :internal, event}

  defp build_action(:postpone), do: :postpone
  defp build_action({:postpone, bool}), do: {:postpone, bool}
end

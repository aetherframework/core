defmodule Aether.Core.PubSub.Bus do
  @moduledoc """
  Event Bus module
  """

  import Aether.Core.Utils.Set

  alias Aether.Core.PubSub
  alias Aether.Core.PubSub.{Dispatcher, Event}

  require Logger

  defstruct topics: %{}, subscribers: %{}, dispatcher: nil

  @type t :: %__MODULE__{
          topics: %{required(PubSub.topic()) => MapSet.t(pid())},
          subscribers: %{required(pid()) => MapSet.t(PubSub.subscriber())},
          dispatcher: struct() | map()
        }

  @doc """
  Creates a new PubSub Bus struct

  ## Examples

    iex> Aether.Core.PubSub.Bus.new()
    #Aether.Core.PubSub.Bus<topics: [], subscribers: [], dispatcher: #Aether.Core.PubSub.Dispatcher.Sync>

  """
  def new, do: %__MODULE__{dispatcher: Dispatcher.Sync.new()}

  @doc """
  Creates a new PubSub Bus with the passed dispatcher

  ## Examples

      iex> dispatcher = Aether.Core.PubSub.Dispatcher.Async.new(MyTaskSupervisor)
      iex> Aether.Core.PubSub.Bus.new(dispatcher)
      #Aether.Core.PubSub.Bus<topics: [], subscribers: [], dispatcher: #Aether.Core.PubSub.Dispatcher.Async>

  """
  def new(dispatcher), do: %__MODULE__{dispatcher: dispatcher}

  @doc """
  Gets all registered topics

  ## Examples

      iex> bus = Aether.Core.PubSub.Bus.new()
      iex> Aether.Core.PubSub.Bus.topics(bus)
      []
      iex> bus = Aether.Core.PubSub.Bus.subscribe(bus, :foo, :c.pid(0, 0, 0), fn _ -> :ok end)
      iex> Aether.Core.PubSub.Bus.topics(bus)
      [:foo]

  """
  def topics(%__MODULE__{} = bus), do: Map.keys(bus.topics)

  @doc """
  Gets all topics pid is subscribed to

  ## Examples

      iex> pid = :c.pid(0, 0, 0)
      iex> bus = Aether.Core.PubSub.Bus.new()
      iex> bus = Aether.Core.PubSub.Bus.subscribe(bus, :foo, pid, fn _ -> :ok end)
      iex> Aether.Core.PubSub.Bus.topics(bus, pid)
      [:foo]

  """
  def topics(%__MODULE__{} = bus, pid) when is_pid(pid),
    do: bus |> get_topics_for_sub(pid)

  @doc """
  Subscribes a pid and handler to a given topic

  ## Examples

      iex> bus = Aether.Core.PubSub.Bus.new()
      iex> bus = Aether.Core.PubSub.Bus.subscribe(bus, :foo, :c.pid(0, 0, 0), {:send, :foo})
      iex> bus.subscribers |> Map.values() |> List.first()
      #MapSet<[foo: {:send, :foo}]>

  """
  def subscribe(bus, topic, pid, handler),
    do: bus |> add_topic_sub(topic, pid) |> add_sub_info(pid, topic, handler)

  @doc """
  Gets the subscriptions for the passed pid

  ## Examples

      iex> pid = :c.pid(0, 0, 0)
      iex> bus = Aether.Core.PubSub.Bus.new()
      iex> bus = Aether.Core.PubSub.Bus.subscribe(bus, :foo, pid, {:send, :foo})
      iex> Aether.Core.PubSub.Bus.subscriptions(bus, pid)
      [:foo]

  """
  def subscriptions(%__MODULE__{} = %{subscribers: subs}, pid),
    do: subs |> Map.get(pid, []) |> Enum.map(fn {t, _} -> t end)

  @doc """
  Unsubscribes a pid from every topic subscribeed

  ## Examples

      iex> pid = :c.pid(0, 0, 0)
      iex> bus = Aether.Core.PubSub.Bus.new()
      iex> bus = Aether.Core.PubSub.Bus.subscribe(bus, :foo, pid, {:send, :foo})
      iex> bus = Aether.Core.PubSub.Bus.subscribe(bus, :bar, pid, {:send, :bar})
      iex> Aether.Core.PubSub.Bus.subscriptions(bus, pid)
      [:bar, :foo]
      iex> bus = Aether.Core.PubSub.Bus.unsubscribe(bus, pid)
      iex> Aether.Core.PubSub.Bus.subscriptions(bus, pid)
      []

  """
  def unsubscribe(%__MODULE__{} = bus, pid),
    do: unsubscribe(bus, pid, topics(bus))

  @doc """
  Unsubscribes a pid from the passed topics

  ## Examples

      iex> pid = :c.pid(0, 0, 0)
      iex> bus = Aether.Core.PubSub.Bus.new()
      iex> bus = Aether.Core.PubSub.Bus.subscribe(bus, :foo, pid, {:send, :foo})
      iex> bus = Aether.Core.PubSub.Bus.subscribe(bus, :bar, pid, {:send, :bar})
      iex> Aether.Core.PubSub.Bus.subscriptions(bus, pid)
      [:bar, :foo]
      iex> bus = Aether.Core.PubSub.Bus.unsubscribe(bus, pid, :bar)
      iex> Aether.Core.PubSub.Bus.subscriptions(bus, pid)
      [:foo]
      iex> bus = Aether.Core.PubSub.Bus.unsubscribe(bus, pid, [:foo])
      iex> Aether.Core.PubSub.Bus.subscriptions(bus, pid)
      []

  """
  def unsubscribe(%__MODULE__{} = bus, pid, topic) when not is_list(topic),
    do: unsubscribe(bus, pid, [topic])

  def unsubscribe(%__MODULE__{} = bus, pid, topics),
    do: bus |> remove_topic_sub(pid, topics) |> remove_sub_info(pid, topics)

  @doc """
  Dispatches event to topic subscribers

  ## Examples

      iex> pid = :c.pid(0, 0, 0)
      iex> bus = Aether.Core.PubSub.Bus.new()
      iex> bus = Aether.Core.PubSub.Bus.subscribe(bus, :foo, pid, fn _ -> :ok end)
      iex> evt = Aether.Core.PubSub.Event.new(:foo, :payload)
      iex> Aether.Core.PubSub.Bus.dispatch(bus, evt)
      [:ok]

  """
  def dispatch(%__MODULE__{} = bus, %Event{topic: topic} = event) do
    handlers =
      bus.topics
      |> Map.get(topic, [])
      |> Enum.map(&get_sub_handlers(bus.subscribers, &1, topic))
      |> List.flatten()
      |> Enum.map(fn {pid, handler} ->
        fn -> dispatch_event(pid, handler, event) end
      end)

    Dispatcher.dispatch(bus.dispatcher, handlers)
  end

  defp add_topic_sub(%__MODULE__{topics: topics} = bus, topic, pid),
    do: %__MODULE__{bus | topics: add_topic_sub(topics, topic, pid)}

  defp add_topic_sub(topics, topic, pid),
    do: Map.update(topics, topic, new_set(pid), &put_set(&1, pid))

  defp add_sub_info(%__MODULE__{subscribers: subscribers} = bus, pid, topic, handler),
    do: %__MODULE__{bus | subscribers: add_sub_info(subscribers, pid, {topic, handler})}

  defp add_sub_info(subscribers, pid, subscriber),
    do: Map.update(subscribers, pid, new_set(subscriber), &put_set(&1, subscriber))

  defp remove_topic_sub(%__MODULE__{topics: topics} = bus, pid, remove),
    do: %__MODULE__{bus | topics: remove_topic_sub(topics, pid, remove)}

  defp remove_topic_sub(topics, pid, remove) when is_list(remove),
    do: Enum.reduce(remove, topics, &remove_topic_sub(&2, pid, &1))

  defp remove_topic_sub(topics, pid, topic),
    do: Map.update(topics, topic, new_set(), &reject_set(&1, fn p -> p == pid end))

  defp remove_sub_info(%__MODULE__{subscribers: subscribers} = bus, pid, topics),
    do: %__MODULE__{bus | subscribers: remove_sub_info(subscribers, pid, topics)}

  defp remove_sub_info(subscribers, pid, topics) do
    remove = fn {t, _} -> Enum.member?(topics, t) end
    subscribers = Map.update(subscribers, pid, new_set(), &reject_set(&1, remove))

    case subscribers |> Map.get(pid, new_set()) |> MapSet.to_list() do
      [] -> Map.drop(subscribers, [pid])
      _ -> subscribers
    end
  end

  defp get_topics_for_sub(%__MODULE__{subscribers: subs}, pid),
    do: subs |> Map.get(pid, []) |> Enum.map(fn {topic, _} -> topic end)

  defp get_handlers_for_topic(handlers, topic) do
    handlers
    |> Enum.filter(fn {t, _} -> t == topic end)
    |> Enum.map(fn {_, h} -> h end)
  end

  defp get_sub_handlers(subs, pid, topic) do
    subs
    |> Map.get(pid, [])
    |> get_handlers_for_topic(topic)
    |> Enum.map(fn handler -> {pid, handler} end)
  end

  defp dispatch_event(pid, :noop, _), do: pid

  defp dispatch_event(pid, {:send, message}, event),
    do: send(pid, {message, event})

  defp dispatch_event(pid, {module, func}, event)
       when is_atom(module) and is_atom(func),
       do: apply(module, func, [%{pid: pid, event: event}])

  defp dispatch_event(pid, handler, event) when is_function(handler, 1),
    do: handler.(%{pid: pid, event: event})

  defp dispatch_event(_pid, _handler, _),
    do: Logger.warn("Invalid Handler -- Ignoring")

  defimpl Inspect do
    import Inspect.Algebra

    def inspect(%{topics: t, subscribers: s, dispatcher: d}, opts) do
      concat([
        "#Aether.Core.PubSub.Bus<",
        "topics: ",
        to_doc(Map.to_list(t), opts),
        ", ",
        "subscribers: ",
        to_doc(Map.to_list(s), opts),
        ", ",
        "dispatcher: ##{inspect(d.__struct__)}",
        ">"
      ])
    end
  end
end

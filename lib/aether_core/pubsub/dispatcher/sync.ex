defmodule Aether.Core.PubSub.Dispatcher.Sync do
  @moduledoc """
  Sync event dispatcher
  """

  alias Aether.Core.PubSub.Dispatcher

  defstruct opts: %{}

  @doc """
  Creates a new sync dispatcher

  ## Examples

      iex> Aether.Core.PubSub.Dispatcher.Sync.new()
      #Aether.Core.PubSub.Dispatcher.Sync<opts: []>

  """
  def new(opts \\ %{}), do: %__MODULE__{opts: opts}

  @doc """
  Dispatches the handlers one after the other

  ## Examples

      iex> dispatcher = Aether.Core.PubSub.Dispatcher.Sync.new()
      iex> handlers = [fn -> :foo end, fn -> :bar end]
      iex> Aether.Core.PubSub.Dispatcher.Sync.dispatch(dispatcher, handlers)
      [:foo, :bar]

  """
  def dispatch(_dispatcher, handlers) when is_list(handlers),
    do: handlers |> Enum.map(fn handler -> handler.() end)

  def dispatch(_, _),
    do: raise(ArgumentError, message: "Invalid value for handlers passed")

  defimpl Dispatcher do
    alias Aether.Core.PubSub.Dispatcher

    def dispatch(dispatcher, handlers),
      do: Dispatcher.Sync.dispatch(dispatcher, handlers)
  end

  defimpl Inspect do
    import Inspect.Algebra

    def inspect(dispatcher, opts) do
      concat([
        "#Aether.Core.PubSub.Dispatcher.Sync<opts: ",
        to_doc(Map.to_list(dispatcher.opts), opts),
        ">"
      ])
    end
  end
end

defmodule Aether.Core.Service.Resolver.Process do
  @moduledoc """
  Service Resolver mixin for service processes, adds
  the same functions that importing `Aether.Core.Service.Resolver` would,
  with the difference being in addition to importing the same functions,
  it will import all then functions with arity - 1 wrappers with
  don't require you to pass the `Service.Group` name as the first
  argument by saving the group name in the process registry.

  For this to work, the importing process module in it's init function
  (preferably one of the first lines) must call `service_group_set("foo")`.

  ## Examples

      defmodule MyStageProcess do
        import Aether.Core.Service.Resolver.Process

        use GenStage

        def init([service_group: service_group] = _opts) do
          service_group_set(service_group)

          # ... rest of init
        end

        # then in some callback get a service's pid
        def handle_call(:foo, _from, _state) do
          {:ok, bar_service} = resolve_service("bar")
          # instead of resolve_service(state.service_group, "bar")
        end
      end

  """

  alias Aether.Core.Service.Resolver

  @process_reg_key :"$aether_service_group"

  def service_group_set(group_name),
    do: Process.put(@process_reg_key, group_name)

  def service_group,
    do: Process.get(@process_reg_key)

  def resolve_service(service_path),
    do: service_group() |> Resolver.resolve_service(service_path)

  def resolve_service!(service_path) do
    {:ok, pid} = resolve_service(service_path)
    pid
  end

  def via_service_group(service_path),
    do: service_group() |> Resolver.via_service_group(service_path)

  defdelegate sigil_p(path, opts), to: Resolver
  defdelegate resolve_service(service_group, servie_path), to: Resolver
  defdelegate via_service_group(service_group, service_path), to: Resolver
end

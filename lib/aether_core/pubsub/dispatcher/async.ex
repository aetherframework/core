defmodule Aether.Core.PubSub.Dispatcher.Async do
  @moduledoc """
  Async event dispatcher
  """

  alias Aether.Core.PubSub.Dispatcher
  alias Aether.Core.Service

  defstruct supervisor: nil, opts: %{}

  @doc """
  Creates a new async dispatcher

  ## Examples

      iex> Aether.Core.PubSub.Dispatcher.Async.new(MyTaskSupervisor)
      #Aether.Core.PubSub.Dispatcher.Async<supervisor: MyTaskSupervisor>

  Or you can pass the `Task.Supervisor` pid

      iex> Aether.Core.PubSub.Dispatcher.Async.new(:c.pid(0, 0, 0))
      #Aether.Core.PubSub.Dispatcher.Async<supervisor: #PID<0.0.0>>

  """
  def new(sup, opts \\ %{}) when is_pid(sup) or is_atom(sup) or is_tuple(sup),
    do: %__MODULE__{supervisor: sup, opts: opts}

  @doc """
  Dispatches the handlers asynchronously

  ## Examples

      iex> {:ok, pid} = Task.Supervisor.start_link()
      iex> dispatcher = Aether.Core.PubSub.Dispatcher.Async.new(pid)
      iex> handlers = [fn -> :foo end, fn -> :bar end]
      iex> tasks = Aether.Core.PubSub.Dispatcher.Async.dispatch(dispatcher, handlers)
      iex> Enum.map(tasks, &Task.await/1)
      [:foo, :bar]

  """
  def dispatch(%__MODULE__{supervisor: sup}, handlers) when is_list(handlers) do
    sup
    |> task_supervisor()
    |> raise_if_dead_supervisor()
    |> dispatch_async(handlers)
  end

  def dispatch(_, _),
    do: raise(ArgumentError, message: "Invalid value for handlers passed")

  defp task_supervisor(pid) when is_pid(pid), do: pid
  defp task_supervisor(name) when is_atom(name), do: Process.whereis(name)
  defp task_supervisor({:service, group, path}), do: Service.lookup!(group, path)

  defp raise_if_dead_supervisor(pid) when is_pid(pid),
    do: if(Process.alive?(pid), do: pid, else: dead_task_supervisor())

  defp raise_if_dead_supervisor(_),
    do: dead_task_supervisor()

  defp dispatch_async(sup, handlers),
    do: handlers |> Enum.map(&Task.Supervisor.async_nolink(sup, &1))

  defp dead_task_supervisor,
    do:
      raise(RuntimeError,
        message: "Aether.Core.PubSub.Dispatcher.Async task supervisor is not alive"
      )

  defimpl Dispatcher do
    alias Aether.Core.PubSub.Dispatcher

    def dispatch(dispatcher, handlers),
      do: Dispatcher.Async.dispatch(dispatcher, handlers)
  end

  defimpl Inspect do
    import Inspect.Algebra

    def inspect(dispatcher, _3opts) do
      concat([
        "#Aether.Core.PubSub.Dispatcher.Async<supervisor: ",
        inspect(dispatcher.supervisor),
        ">"
      ])
    end
  end
end

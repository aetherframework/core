defmodule Aether.Core.PubSub.Event do
  @moduledoc """
  Event module
  """

  alias Aether.Core.PubSub

  @enforce_keys [:topic]
  defstruct [:topic, :payload]

  @type t :: %__MODULE__{topic: PubSub.topic(), payload: any()}

  @doc """
  Creates a new PubSub Event

  ## Examples

      iex> Aether.Core.PubSub.Event.new({:foo, :bar}, "baz")
      #Aether.Core.PubSub.Event<topic: {:foo, :bar}, payload: "baz">

      iex> Aether.Core.PubSub.Event.new({:foo, :bar})
      #Aether.Core.PubSub.Event<topic: {:foo, :bar}, payload: nil>

      iex> Aether.Core.PubSub.Event.new(nil)
      ** (ArgumentError) topic must be supplied

      iex> Aether.Core.PubSub.Event.new(nil, "bax")
      ** (ArgumentError) topic must be supplied

  """
  def new(topic, payload \\ nil)

  def new(topic, _payload) when is_nil(topic),
    do: raise(ArgumentError, message: "topic must be supplied")

  def new(topic, payload),
    do: %__MODULE__{topic: topic, payload: payload}

  defimpl Inspect do
    import Inspect.Algebra

    def inspect(%{topic: t, payload: p}, opts) do
      concat([
        "#Aether.Core.PubSub.Event<",
        "topic: ",
        to_doc(t, opts),
        ", ",
        "payload: ",
        to_doc(p, opts),
        ">"
      ])
    end
  end
end

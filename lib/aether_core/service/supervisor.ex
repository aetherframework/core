defmodule Aether.Core.Service.Supervisor do
  @moduledoc """
  PubSub supervisor
  """

  alias Aether.Core.Service.Registrar

  use Supervisor

  def start_link(opts \\ []) do
    Supervisor.start_link(__MODULE__, opts, name: __MODULE__)
  end

  def init(_opts) do
    children = [
      {Registrar.Server, name: Registrar.Server}
    ]

    Supervisor.init(children, strategy: :rest_for_one)
  end
end

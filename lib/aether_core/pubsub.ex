defmodule Aether.Core.PubSub do
  @moduledoc """
  PubSub
  """

  alias Aether.Core.PubSub.Server

  defdelegate subscribe(server, topic, handler), to: Server
  defdelegate unsubscribe(server), to: Server
  defdelegate unsubscribe(server, topics), to: Server
  defdelegate dispatch(server, event), to: Server
  defdelegate dispatch(server, topic, payload), to: Server
  defdelegate topics(server), to: Server
  defdelegate topics(server, pid), to: Server
  defdelegate state(server), to: Server
end

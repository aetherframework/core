defmodule Aether.Core.Configurable.Parser do
  @moduledoc """
  Field parsers
  """

  def function(func), do: parse_func(func)
  def function(func, arity), do: parse_func(func, arity)

  defp parse_func(value) when is_function(value),
    do: {:ok, value}

  defp parse_func(value) when not is_function(value),
    do: {:error, "not a function"}

  defp parse_func(value, arity) when is_function(value, arity),
    do: {:ok, value}

  defp parse_func(value, arity)
       when is_function(value) and not is_function(value, arity),
       do: {:error, "function does not have arity of #{arity}"}

  defp parse_func(value, _arity) when not is_function(value),
    do: {:error, "not a function"}
end

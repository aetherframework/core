defmodule Aether.Core.Service do
  @moduledoc """
  Service module
  """

  alias Aether.Core.Service.Resolver

  @type path :: String.t()

  def lookup({group, path}), do: lookup(group, path)
  def lookup(group, path), do: Resolver.resolve_service(group, path)

  def lookup!({group, path}), do: lookup!(group, path)
  def lookup!(group, path), do: Resolver.resolve_service!(group, path)
end

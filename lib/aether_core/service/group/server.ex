defmodule Aether.Core.Service.Group.Server do
  @moduledoc """
  Service Group Server
  """

  alias Aether.Core.Service.{Group, Registrar}

  use GenServer

  def start_link(group: group) do
    case GenServer.start_link(__MODULE__, group) do
      {:ok, pid} ->
        case Registrar.register(group.name, pid) do
          :ok -> {:ok, pid}
          _ -> {:error, "Could not register group with Registrar"}
        end

      value ->
        value
    end
  end

  def init(%Group{} = group) do
    Process.flag(:trap_exit, true)

    registry = Group.Registry.new()

    {:ok, {group, registry}}
  end

  def state(server),
    do: GenServer.call(server, :state)

  def name(server),
    do: GenServer.call(server, :name)

  def register(server, service, pid),
    do: GenServer.cast(server, {:register, service, pid})

  def unregister(server, service),
    do: GenServer.cast(server, {:unregister, service})

  def lookup(server, service),
    do: GenServer.call(server, {:lookup, service})

  def handle_call(:state, _from, state),
    do: {:reply, state, state}

  def handle_call(:name, _from, {group, _registry} = state),
    do: {:reply, group.name, state}

  def handle_call({:lookup, service}, _from, {_group, registry} = state) do
    {pid, _} = Group.Registry.lookup(registry, service)

    {:reply, pid, state}
  end

  def handle_cast({:register, service, pid}, {group, registry}) do
    {_, registry} = Group.Registry.register(registry, {service, pid})

    {:noreply, {group, registry}}
  end

  def handle_cast({:unregister, service}, {group, registry}),
    do: {:noreply, {group, Group.Registry.unregister(registry, service)}}

  def handle_info({:DOWN, ref, :process, _pid, _reason}, {group, registry}),
    do: {:noreply, {group, Group.Registry.unregister(registry, ref)}}
end

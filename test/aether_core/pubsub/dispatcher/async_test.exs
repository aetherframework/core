defmodule Aether.Core.PubSub.Dispatcher.AsyncTest do
  @moduledoc false

  use ExUnit.Case, async: true

  alias Aether.Core.PubSub.Dispatcher
  alias Aether.Core.PubSub.Dispatcher.Async

  doctest Async

  setup do
    pid = start_supervised!(Task.Supervisor)
    dispatcher = Async.new(pid)

    {:ok, %{dispatcher: dispatcher}}
  end

  describe "Aether.Core.PubSub.Dispatcher.Aync.dispatch/2" do
    test "returns empty list when handlers list is empty", %{dispatcher: disp} do
      result = Async.dispatch(disp, [])

      assert result == []
    end

    test "returns list of tasks", %{dispatcher: disp} do
      handlers = [fn -> :foo end, fn -> :bar end]
      result = Async.dispatch(disp, handlers)

      assert Enum.all?(result, &match?(%Task{}, &1))
      assert Enum.map(result, &Task.await/1) == [:foo, :bar]
    end

    test "raises ArgumentError when passed invalid handlers value", %{dispatcher: disp} do
      assert_raise ArgumentError, "Invalid value for handlers passed", fn ->
        Async.dispatch(disp, :invalid)
      end
    end
  end

  describe "Aether.Core.PubSub.Dispatcher.dispatch/2" do
    test "returns empty list when handlers list is empty", %{dispatcher: disp} do
      result = Dispatcher.dispatch(disp, [])

      assert result == []
    end

    test "returns list of tasks", %{dispatcher: disp} do
      handlers = [fn -> :foo end, fn -> :bar end]
      result = Async.dispatch(disp, handlers)

      assert Enum.all?(result, &match?(%Task{}, &1))
      assert Enum.map(result, &Task.await/1) == [:foo, :bar]
    end

    test "raises ArgumentError when passed invalid handlers value", %{dispatcher: disp} do
      assert_raise ArgumentError, "Invalid value for handlers passed", fn ->
        Dispatcher.dispatch(disp, :invalid)
      end
    end
  end
end

defmodule Aether.Core.Service.Registrar.Server do
  @moduledoc """
  Service Registrar Server
  """

  alias Aether.Core.Service.{Group, Registrar}

  use GenServer

  def start_link([]),
    do: start_link(name: __MODULE__)

  def start_link([name: name] = opts) do
    GenServer.start_link(__MODULE__, Keyword.delete(opts, :name), name: name)
  end

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts)
  end

  def init(_opts) do
    Process.flag(:trap_exit, true)

    registry = Registrar.Registry.new()

    {:ok, registry}
  end

  def state(server),
    do: GenServer.call(server, :state)

  def register(server, service, pid),
    do: GenServer.cast(server, {:register, service, pid})

  def unregister(server, service),
    do: GenServer.cast(server, {:unregister, service})

  def lookup(server, group),
    do: GenServer.call(server, {:lookup, group})

  #################
  # Via Functions #
  #################

  def whereis_name({server, {_group, _service} = path}),
    do: GenServer.call(server, {:whereis, path})

  def whereis_name({server, group, service}),
    do: GenServer.call(server, {:whereis, {group, service}})

  def register_name({_registrar, path}, _pid) when not is_tuple(path),
    do: raise(ArgumentError, message: "Missing service name")

  def register_name({server, {group, service}}, pid),
    do: register_name(server, group, service, pid)

  def register_name({server, group, service}, pid),
    do: register_name(server, group, service, pid)

  def register_name(server, group, service, pid),
    do: GenServer.call(server, {:register_service, group, service, pid})

  def send({server, group}, msg) do
    case Registrar.Registry.lookup(server, group) do
      {pid, _} when is_pid(pid) -> Kernel.send(pid, msg)
      {nil, nil} -> :erlang.error(:badarg, [{server, group}, msg])
    end
  end

  def unregister_name({server, {group, service}}),
    do: GenServer.cast(server, {:unregister_service, group, service})

  def handle_call(:state, _from, state),
    do: {:reply, state, state}

  def handle_call({:lookup, group}, _from, registry) do
    {pid, _} = Registrar.Registry.lookup(registry, group)

    {:reply, pid, registry}
  end

  def handle_call({:whereis, {name, service}}, _from, registry) do
    {group, _} = Registrar.Registry.lookup(registry, name)

    case Group.Server.lookup(group, service) do
      pid when is_pid(pid) -> {:reply, pid, registry}
      _ -> {:reply, :undefined, registry}
    end
  end

  def handle_call({:register_service, name, service, pid}, _from, state) do
    {group, _} = Registrar.Registry.lookup(state, name)

    Group.Server.register(group, service, pid)

    {:reply, :yes, state}
  end

  def handle_cast({:register, service, pid}, registry) do
    {_, registry} = Registrar.Registry.register(registry, {service, pid})

    {:noreply, registry}
  end

  def handle_cast({:unregister, service}, registry),
    do: {:noreply, Registrar.Registry.unregister(registry, service)}

  def handle_cast({:unregister_service, name, service}, registry) do
    {group, _} = Registrar.Registry.lookup(registry, name)

    Group.Server.unregister(group, service)

    {:noreply, registry}
  end

  def handle_info({:DOWN, ref, :process, _pid, _reason}, registry),
    do: {:noreply, Registrar.Registry.unregister(registry, ref)}
end

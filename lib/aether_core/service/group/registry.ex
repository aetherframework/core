defmodule Aether.Core.Service.Group.Registry do
  @moduledoc """
  Service Group Registry struct
  """

  @derive [Inspect]

  defstruct refs: %{}, services: %{}

  def new, do: %__MODULE__{}

  def registered?(%{services: services}, service),
    do: Map.has_key?(services, service)

  def register(registry, {path, pid}) do
    case registered?(registry, path) do
      true -> {:exists, registry}
      false -> {:ok, register_service(registry, {path, pid})}
    end
  end

  def lookup(%{services: services}, path),
    do: Map.get(services, path, {nil, nil})

  def unregister(%{refs: refs} = registry, ref) when is_reference(ref),
    do: unregister(registry, Map.get(refs, ref))

  def unregister(registry, path) do
    {_, ref} = lookup(registry, path)

    Process.demonitor(ref)

    registry
    |> remove_ref(ref)
    |> remove_service(path)
  end

  defp register_service(registry, {path, pid}) do
    ref = Process.monitor(pid)

    registry
    |> add_ref(ref, path)
    |> add_service(path, {pid, ref})
  end

  defp add_ref(%{refs: refs} = reg, ref, path),
    do: refs |> Map.put(ref, path) |> (&Map.put(reg, :refs, &1)).()

  defp add_service(%{services: services} = reg, path, {pid, ref}),
    do: services |> Map.put(path, {pid, ref}) |> (&Map.put(reg, :services, &1)).()

  defp remove_ref(%{refs: refs} = reg, ref),
    do: refs |> Map.delete(ref) |> (&Map.put(reg, :refs, &1)).()

  defp remove_service(%{services: services} = reg, path),
    do: services |> Map.delete(path) |> (&Map.put(reg, :services, &1)).()
end

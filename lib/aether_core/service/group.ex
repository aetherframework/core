defmodule Aether.Core.Service.Group do
  @moduledoc """
  Service Group module
  """

  alias __MODULE__
  alias __MODULE__.{Server}

  @derive [Inspect]

  @enforce_keys [:name]
  defstruct name: nil

  defdelegate name(group), to: Server
  defdelegate state(group), to: Server
  defdelegate register(group, service, pid), to: Server
  defdelegate unregister(group, service), to: Server
  defdelegate lookup(group, service), to: Server

  def new, do: 12 |> Nanoid.generate() |> new()

  def new(name), do: %Group{name: name}
end

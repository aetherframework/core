defmodule Aether.Core.Utils.TSV do
  @moduledoc false

  NimbleCSV.define(TSVParser, separator: "\t")

  @parser TSVParser
  @read_ahead 100_000

  def stream(file), do: stream(file, headers: nil)

  def stream(file, headers: headers) do
    stream =
      file
      |> Path.expand()
      |> File.stream!(read_ahead: @read_ahead)
      |> @parser.parse_stream()
      |> Stream.reject(&(&1 == [""]))
      |> Stream.map(&set_keys(&1, headers))

    {:ok, stream}
  rescue
    File.Error -> {:error, :file_error, file}
    _ -> {:error, :unknown_error, file}
  end

  defp set_keys(columns, nil), do: columns

  defp set_keys(columns, headers),
    do: headers |> Enum.zip(columns) |> Enum.into(%{})
end

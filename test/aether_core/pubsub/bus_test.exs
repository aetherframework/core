defmodule Aether.Core.PubSub.BusDispatchTests do
  @moduledoc false

  use ExUnit.CaseTemplate

  alias Aether.Core.PubSub.{Bus, Dispatcher, Event}

  using opts do
    quote do
      alias Aether.Core.PubSub.{Bus, Dispatcher, Event}

      dispatcher = Keyword.get(unquote(opts), :dispatcher, :sync)

      describe "#{dispatcher} dispatcher" do
        @tag dispatcher: dispatcher
        test "dispatches to subscriber handlers with {:send, message} handler", %{bus: bus} do
          bus = Bus.subscribe(bus, :test, self(), {:send, :test_event})
          event = Event.new(:test, :payload)

          Bus.dispatch(bus, event)

          assert_receive {:test_event, ^event}
        end

        @tag dispatcher: dispatcher
        test "dispatches to subscriber handlers with function handler", %{bus: bus} do
          handler = fn %{pid: pid, event: event} ->
            send(pid, {:test_event, event})
          end

          bus = Bus.subscribe(bus, :test, self(), handler)
          event = Event.new(:test, :payload)

          Bus.dispatch(bus, event)

          assert_receive {:test_event, ^event}
        end

        @tag dispatcher: dispatcher
        test "dispatches to subscriber handlers with {module, function} handler", %{bus: bus} do
          handler = {Aether.Core.PubSub.BusDispatchTests, :handler_func}

          bus = Bus.subscribe(bus, :test, self(), handler)
          event = Event.new(:test, :payload)

          Bus.dispatch(bus, event)

          assert_receive {:test_event, ^event}
        end
      end
    end
  end

  def handler_func(%{pid: pid, event: event}),
    do: send(pid, {:test_event, event})
end

defmodule Aether.Core.PubSub.BusTest do
  @moduledoc false

  use ExUnit.Case, async: true

  use Aether.Core.PubSub.BusDispatchTests, dispatcher: :sync
  use Aether.Core.PubSub.BusDispatchTests, dispatcher: :async

  setup context do
    bus =
      case Map.get(context, :dispatcher, :sync) do
        :sync ->
          Bus.new()

        :async ->
          Task.Supervisor
          |> start_supervised!()
          |> Dispatcher.Async.new()
          |> Bus.new()
      end

    %{bus: bus}
  end

  doctest Bus

  describe "empty bus" do
    test "Aether.Core.PubSub.Bus.topics/1", %{bus: bus} do
      assert Bus.topics(bus) == []
    end

    test "Aether.Core.PubSub.Bus.topics/2", %{bus: bus} do
      assert Bus.topics(bus, self()) == []
    end

    test "Aether.Core.PubSub.Bus.subscribe/4", %{bus: bus} do
      topic = :test
      handler = {:send, :test_msg}
      subscription = {topic, handler}

      bus = Bus.subscribe(bus, topic, self(), handler)

      assert Bus.topics(bus) == [topic]
      assert Bus.topics(bus, self()) == [topic]
      assert Map.get(bus.subscribers, self()) == MapSet.new([subscription])
      assert Bus.subscriptions(bus, self()) == [topic]
    end

    test "Aether.Core.PubSub.Bus.unsubscribe/2", %{bus: bus} do
      bus = Bus.unsubscribe(bus, self())

      assert Bus.topics(bus) == []
      assert bus.subscribers == %{}
    end

    test "Aether.Core.PubSub.Bus.unsubscribe/3", %{bus: bus} do
      bus = Bus.unsubscribe(bus, self(), [:test])

      assert Bus.topics(bus) == [:test]
      assert bus.subscribers == %{}
    end
  end

  describe "bus with subscribers" do
    setup :setup_test_subscribers

    test "Aether.Core.PubSub.Bus.topics/1", %{bus: bus} do
      assert Bus.topics(bus) == [:bar, :foo]
    end

    test "Aether.Core.PubSub.Bus.topics/2", %{bus: bus} do
      assert Bus.topics(bus, self()) == [:bar, :foo]
    end

    test "Aether.Core.PubSub.Bus.subscribe/4", %{bus: bus} do
      topic = :test
      handler = {:send, :test_msg}

      bus = Bus.subscribe(bus, topic, self(), handler)

      assert Bus.topics(bus) == [:bar, :foo, topic]
      assert Bus.topics(bus, self()) == [:bar, :foo, topic]
      assert Bus.subscriptions(bus, self()) == [:bar, :foo, topic]
    end

    test "Aether.Core.PubSub.Bus.unsubscribe/2", %{bus: bus} do
      bus = Bus.unsubscribe(bus, self())

      assert Bus.topics(bus) == [:bar, :foo]
      assert bus.subscribers == %{}
    end

    test "Aether.Core.PubSub.Bus.unsubscribe/3 with single topic", %{bus: bus} do
      bus = Bus.unsubscribe(bus, self(), :bar)

      assert Bus.topics(bus) == [:bar, :foo]
      assert Bus.subscriptions(bus, self()) == [:foo]
    end

    test "Aether.Core.PubSub.Bus.unsubscribe/3", %{bus: bus} do
      bus = Bus.unsubscribe(bus, self(), [:bar])

      assert Bus.topics(bus) == [:bar, :foo]
      assert Bus.subscriptions(bus, self()) == [:foo]
    end
  end

  defp setup_test_subscribers(%{bus: bus}) do
    foo_handler = fn %{pid: pid, event: event} ->
      send(pid, {:foo_event, event})
    end

    bar_handler = fn %{pid: pid, event: event} ->
      send(pid, {:bar_event, event})
    end

    bus = Bus.subscribe(bus, :foo, self(), foo_handler)
    bus = Bus.subscribe(bus, :bar, self(), bar_handler)

    %{bus: bus}
  end
end

defmodule Aether.Core.Utils.Stream do
  @moduledoc false

  @doc """
  Used to take `n` items from a stream and return them along
  with a new stream with the remaining items, if the stream
  is empty, it will return an empty list

  ## Examples

      iex> {:ok, {items, stream}} = Aether.Core.Utils.Stream.take(1..20, 10)
      iex> items |> Enum.to_list() |> length()
      10
      iex> stream |> Enum.to_list() |> length()
      10
      iex> {:ok, {items, stream}} = Aether.Core.Utils.Stream.take(stream, 15)
      iex> items |> Enum.to_list() |> length()
      10
      iex> stream |> Enum.to_list() |> length()
      0
      iex> {:ok, {[], stream}} = Aether.Core.Utils.Stream.take(stream, 20)
      iex> stream |> Enum.to_list() |> length()
      0

      iex> {:error, error} = Aether.Core.Utils.Stream.take(nil, 5)
      iex> raise error
      ** (ArgumentError) Invalid value for stream. Enumerable not implemented for nil.

      iex> {:error, error} = Aether.Core.Utils.Stream.take([], :a)
      iex> raise error
      ** (ArgumentError) Invalid value for count.

  """
  def take(stream, count) do
    items = Enum.take(stream, count)
    updated = Stream.drop(stream, count)

    if Enum.take(updated, 1) == [] do
      {:ok, {items, []}}
    else
      {:ok, {items, updated}}
    end
  rescue
    e in Protocol.UndefinedError ->
      proto = "#{e.protocol}" |> String.replace_prefix("Elixir.", "")
      value = if e.value == nil, do: "nil", else: e.value
      msg = "Invalid value for stream. #{proto} not implemented for #{value}."

      {:error, %ArgumentError{message: msg}}

    _e in FunctionClauseError ->
      message = "Invalid value for count."

      {:error, %ArgumentError{message: message}}

    e ->
      {:error, e}
  end
end

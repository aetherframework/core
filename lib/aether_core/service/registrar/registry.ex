defmodule Aether.Core.Service.Registrar.Registry do
  @moduledoc """
  Service Registrar Registry struct
  """

  @derive [Inspect]

  defstruct refs: %{}, groups: %{}

  def new, do: %__MODULE__{}

  def registered?(%{groups: groups}, group),
    do: Map.has_key?(groups, group)

  def register(registry, {group, pid}) do
    case registered?(registry, group) do
      true -> {:exists, registry}
      false -> {:ok, register_group(registry, {group, pid})}
    end
  end

  def lookup(%{groups: groups}, group),
    do: Map.get(groups, group, {nil, nil})

  def unregister(%{refs: refs} = registry, ref) when is_reference(ref),
    do: unregister(registry, Map.get(refs, ref))

  def unregister(registry, group) do
    {_, ref} = lookup(registry, group)

    Process.demonitor(ref)

    registry
    |> remove_ref(ref)
    |> remove_group(group)
  end

  defp register_group(registry, {group, pid}) do
    ref = Process.monitor(pid)

    registry
    |> add_ref(ref, group)
    |> add_group(group, {pid, ref})
  end

  defp add_ref(%{refs: refs} = reg, ref, group),
    do: refs |> Map.put(ref, group) |> (&Map.put(reg, :refs, &1)).()

  defp add_group(%{groups: groups} = reg, group, {pid, ref}),
    do: groups |> Map.put(group, {pid, ref}) |> (&Map.put(reg, :groups, &1)).()

  defp remove_ref(%{refs: refs} = reg, ref),
    do: refs |> Map.delete(ref) |> (&Map.put(reg, :refs, &1)).()

  defp remove_group(%{groups: groups} = reg, group),
    do: groups |> Map.delete(group) |> (&Map.put(reg, :groups, &1)).()
end

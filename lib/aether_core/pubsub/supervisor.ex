defmodule Aether.Core.PubSub.Supervisor do
  @moduledoc """
  PubSub supervisor
  """

  alias Aether.Core.PubSub
  alias Aether.Core.PubSub.Dispatcher
  alias Aether.Core.Service.Resolver

  use Supervisor

  import Resolver

  def start_link(opts) do
    Supervisor.start_link(__MODULE__, opts)
  end

  def init(group: group, dispatcher: :async) do
    task_super_path = ~p/pubsub task_supervisor/
    task_super_name = via_service_group(group, task_super_path)

    dispatcher = Dispatcher.Async.new({:service, group, task_super_path})

    children = [
      {Task.Supervisor, name: task_super_name},
      {PubSub.Server, group: group, dispatcher: dispatcher}
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end

  def init(group: group, dispatcher: :sync), do: init(group: group)

  def init(group: group) do
    dispatcher = Dispatcher.Sync.new()

    children = [
      {PubSub.Server, group: group, dispatcher: dispatcher}
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end

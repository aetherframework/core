defmodule Aether.Core.Service.Registrar do
  @moduledoc """
  Registrar for Service Groups and their ETS Tables
  """

  alias Aether.Core.Service.Registrar.Server

  def state,
    do: Server.state(Server)

  def register(group, pid),
    do: Server.register(Server, group, pid)

  def unregister(group),
    do: Server.unregister(Server, group)

  def lookup(group),
    do: Server.lookup(Server, group)
end
